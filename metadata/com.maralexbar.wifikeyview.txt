Categories:Security
License:GPLv3
Web Site:
Source Code:https://github.com/Maralexbar/Wifi-Key-View
Issue Tracker:https://github.com/Maralexbar/Wifi-Key-View/issues

Auto Name:Wifi Key View
Summary:View saved WiFi passwords
Description:
View saved WiFi passwords. Requires busybox and root access.
.

Requires Root:Yes

Repo Type:git
Repo:https://github.com/Maralexbar/Wifi-Key-View

Build:1.0,1
    commit=67ef5ae5ec20ad1ae28197b135aba3f413863383
    prebuild=sed -i -e "s@su -c cat /data@su -c \'cat /data@g" -e "s@\"\"@\"\'\"@g" src/com/maralexbar/wifikeyview/MainActivity.java

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1


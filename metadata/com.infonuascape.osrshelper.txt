AntiFeatures:NonFreeNet
Categories:Games
License:GPLv3
Web Site:https://github.com/ldionmarcil/OSRSHelper/blob/HEAD/README.md
Source Code:https://github.com/ldionmarcil/OSRSHelper
Issue Tracker:https://github.com/ldionmarcil/OSRSHelper/issues

Auto Name:OSRS Helper
Summary:View your RuneScape stats
Description:
View stats of your [http://oldschool.runescape.com/ OldSchool Runescape]
character.
.

Repo Type:git
Repo:https://github.com/ldionmarcil/OSRSHelper

Build:1.1.1,3
    commit=7c8b3ba8f400e280e36fec17779a295e94a70a5d
    target=android-19

Build:1.2.1,5
    commit=23a83153bb4350841b919528c5550c3be34de79a
    target=android-19
    extlibs=android/android-support-v4.jar

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2.1
Current Version Code:5

